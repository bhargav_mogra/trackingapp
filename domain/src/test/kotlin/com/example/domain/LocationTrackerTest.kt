package com.example.domain

import com.example.domain.background.tasks.LocationTrackingBackgroundTask
import com.example.domain.data.contract.LocationRepository
import com.example.domain.data.contract.ViewStateStorage
import com.example.domain.entity.GpsProviderNotAvailableError
import com.example.domain.entity.Location
import com.example.domain.entity.LocationError
import com.example.domain.entity.LocationPermissionNotGrantedError
import com.nhaarman.mockito_kotlin.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocationTrackerTest {
    private lateinit var locationTracker: LocationTracker
    @Mock
    lateinit var locationRepository: LocationRepository
    @Mock
    lateinit var trackingBgTask: LocationTrackingBackgroundTask
    @Mock
    lateinit var viewStateStorage: ViewStateStorage

    @Before
    fun setUp() {
        locationTracker = LocationTracker(
            locationRepository,
            trackingBgTask, viewStateStorage
        )
    }

    @Test
    fun `test invocation with callback service not running starts bg task`() {
        // arrange
        var gpsEnabledCallbackInvocation = false
        var onLocationAccessPermissionNotGranted = false
        var onLocationChanged = false
        // act
        locationTracker {
            onGpsNotEnabled {
                gpsEnabledCallbackInvocation = true
            }
            onLocationAccessPermissionNotGranted {
                onLocationAccessPermissionNotGranted = true
            }
            onLocationChanged {
                onLocationChanged = true
            }
        }
        // assert
        verify(trackingBgTask).start()
        verify(viewStateStorage).isServiceRunning = true
        assertThat(onLocationChanged).isFalse()
        assertThat(onLocationAccessPermissionNotGranted).isFalse()
        assertThat(gpsEnabledCallbackInvocation).isFalse()
        val captor = argumentCaptor<(Location) -> Unit>()
        val erroCaptor = argumentCaptor<(LocationError) -> Unit>()
        verify(locationRepository).observeLocationUpdates(eq(101),
            captor.capture(), erroCaptor.capture())
        val successcallback = captor.firstValue
        val errorCallback = erroCaptor.firstValue
        successcallback(Location(12.0, 100.0))
        errorCallback(GpsProviderNotAvailableError())
        errorCallback(LocationPermissionNotGrantedError())
        assertThat(onLocationChanged).isTrue()
        assertThat(onLocationAccessPermissionNotGranted).isTrue()
        assertThat(gpsEnabledCallbackInvocation).isTrue()
    }

    @Test
    fun `test invoke`() {
        // act
        locationTracker()
        // assert
        verify(locationRepository).observeLocationUpdates(eq(102), any(), any())
    }
}
