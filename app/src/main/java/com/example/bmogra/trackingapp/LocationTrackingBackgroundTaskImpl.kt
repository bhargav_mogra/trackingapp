package com.example.bmogra.trackingapp

import android.content.Context
import android.content.Intent
import com.example.bmogra.trackingapp.TrackingService.Companion.COMMAND
import com.example.bmogra.trackingapp.TrackingService.Companion.START_TRACKING
import com.example.bmogra.trackingapp.TrackingService.Companion.STOP_TRACKING
import com.example.domain.background.tasks.LocationTrackingBackgroundTask

class LocationTrackingBackgroundTaskImpl(
    private val context: Context
) : LocationTrackingBackgroundTask {
    override fun start() {
        context.startService(Intent(context, TrackingService::class.java).apply {
            putExtra(COMMAND, START_TRACKING)
        })
    }

    override fun stop() {
        context.startService(Intent(context, TrackingService::class.java).apply {
            putExtra(COMMAND, STOP_TRACKING)
        })
    }

}
