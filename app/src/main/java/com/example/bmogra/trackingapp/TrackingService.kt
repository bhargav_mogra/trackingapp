package com.example.bmogra.trackingapp

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import com.example.domain.StartTrackingLocation
import com.example.domain.StopTrackingLocation
import javax.inject.Inject

class TrackingService : Service() {
    companion object {
        const val START_TRACKING = "startTracking"
        const val STOP_TRACKING = "stopTracking"
        const val COMMAND = "command"
        const val NOTIFICATION_CHANNEL_ID = "channel_01"
    }

    @Inject
    lateinit var startTrackingLocation: StartTrackingLocation

    @Inject
    lateinit var stopTrackingLocation: StopTrackingLocation

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        (application as TrackerApplication).appComponent.inject(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            val command: String? = it.extras?.getString(COMMAND)
            when (command) {
                START_TRACKING -> startTracking()

                STOP_TRACKING -> stopTracking()
            }
        }
        return START_STICKY
    }

    private fun startTracking() {
        val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setContentText("Tracking")
            .build()
        startForeground(1001, notification)
        startTrackingLocation()
    }

    private fun stopTracking() {
        stopTrackingLocation()
        stopForeground(true)
        stopSelf()
    }
}
