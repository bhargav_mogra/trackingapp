package com.example.bmogra.trackingapp

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.example.presenter.LocationViewModel
import com.example.presenter.TrackerPresenter
import com.example.presenter.TrackerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class TrackerActivity : AppCompatActivity() {

    @Inject
    lateinit var presenter: TrackerPresenter

    private var trackerViewRef: TrackerViewImpl? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as TrackerApplication)
            .appComponent.add(TrackerActivityModule())
            .inject(this)
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    (map as? SupportMapFragment)?.getMapAsync { googleMap ->
                        if (trackerViewRef == null) trackerViewRef = TrackerViewImpl(googleMap)
                        trackerViewRef?.let { presenter.onViewReady(it) }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    Toast.makeText(
                        applicationContext, "This App requires location access to work properly",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }).check()
        startButton.setOnClickListener {
            presenter.startTracking()
            stopButton.visibility = VISIBLE
            startButton.visibility = GONE
            disableButtonsFor1sec()
        }
        stopButton.setOnClickListener {
            presenter.stopTracking()
            stopButton.visibility = GONE
            startButton.visibility = VISIBLE
            disableButtonsFor1sec()
        }
        syncButton.setOnClickListener {
            presenter.startSync()
        }
    }

    private fun disableButtonsFor1sec() {
        startButton.isEnabled = false
        stopButton.isEnabled = false
        window.decorView.postDelayed({
            startButton.isEnabled = true
            stopButton.isEnabled = true
        }, 1000)
    }

    private inner class TrackerViewImpl(private val googleMap: GoogleMap) : TrackerView {
        init {
            startButton.visibility = VISIBLE
            progressBar.visibility = GONE
        }

        override fun showLastKnownLocation(lastKnownLocation: LocationViewModel) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastKnownLocation.toLatLng(), 13f))
        }

        override fun getUiThreadExecutor(): (Runnable) -> Unit = { Handler(Looper.getMainLooper()).post(it) }

        override fun drawLineOnMap(viewModel: List<LocationViewModel>) {
            val polyline = googleMap.addPolyline(PolylineOptions().width(12f))
            polyline.points = viewModel.toLatLng().toMutableList()
        }

        override fun showGpsNotEnabledError() {
            Toast.makeText(applicationContext, "Gps Not enabled!", Toast.LENGTH_SHORT).show()
        }

        override fun askForLocationPermission() {
            Toast.makeText(
                applicationContext, "Please provide app with location permission",
                Toast.LENGTH_SHORT
            ).show()
        }

        override fun clearMap() {
            googleMap.clear()
        }

        override fun updateToStateRecievingLocationUpdates() {
            startButton.visibility = GONE
            stopButton.visibility = VISIBLE
        }
    }
}

private fun List<LocationViewModel>.toLatLng(): List<LatLng> = map { it.toLatLng() }

private fun LocationViewModel.toLatLng() = LatLng(lat, long)
