package com.example.presenter

import com.example.domain.HandleViewReady
import com.example.domain.StartTrackingLocation
import com.example.domain.StopTrackingLocation
import com.example.domain.SyncLocationHistory
import com.example.domain.entity.Location
import java.lang.ref.WeakReference

class TrackerPresenter(
    private val startTrackingLocation: StartTrackingLocation,
    private val stopTrackingLocation: StopTrackingLocation,
    private val handleViewReady: HandleViewReady,
    private val SyncLocationHistory: SyncLocationHistory
) {
    private var viewRef: WeakReference<TrackerView> = WeakReference<TrackerView>(null)

    fun onViewReady(view: TrackerView) {
        viewRef.clear()
        viewRef = WeakReference(view)

        handleViewReady {
            onReceiveLastKnownLocation { location ->
                view {
                    showLastKnownLocation(location.toViewModel())
                }
            }
            onReceiveLocationUpdates {
                view { updateToStateRecievingLocationUpdates() }
                startTracking()
            }
        }
    }

    fun startTracking() {
        view { clearMap() }
        startTrackingLocation {
            onGpsNotEnabled {
                view { showGpsNotEnabledError() }
            }
            onLocationAccessPermissionNotGranted {
                view { askForLocationPermission() }
            }
            onLocationChanged {
                view { drawLineOnMap(it.toViewModels()) }
            }
        }
    }

    fun stopTracking() {
        stopTrackingLocation()
    }

    private fun <R> view(runBlock: TrackerView.() -> R): R? = viewRef.get()?.run(runBlock)

    private fun Location.toViewModel(): LocationViewModel = LocationViewModel(lat, long)

    private fun List<Location>.toViewModels(): List<LocationViewModel> = map { it.toViewModel() }

    fun startSync() {
        SyncLocationHistory()
    }
}
