package com.example.data

import com.example.domain.data.contract.ViewStateStorage

class ViewStateStorageImpl() : ViewStateStorage {
    override var isServiceRunning: Boolean = false
}
