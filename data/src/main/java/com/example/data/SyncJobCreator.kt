package com.example.data

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator

class SyncJobCreator: JobCreator {
    override fun create(tag: String): Job? {
        when (tag) {
            LocationHistorySyncJob.TAG -> LocationHistorySyncJob()
            else -> null
        }
    }
}
