package com.example.domain

import com.example.domain.background.tasks.LocationTrackingBackgroundTask
import com.example.domain.data.contract.LocationRepository
import com.example.domain.data.contract.ViewStateStorage
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocationTrackStopperTest {
    private lateinit var locationTrackStopper: LocationTrackStopper
    @Mock
    lateinit var locationRepository: LocationRepository
    @Mock
    lateinit var trackingBgTask: LocationTrackingBackgroundTask
    @Mock
    lateinit var viewStateStorage: ViewStateStorage

    @Before
    fun setUp() {
        locationTrackStopper = LocationTrackStopper(locationRepository, trackingBgTask, viewStateStorage)
    }

    @Test
    fun `test when service is running stops bg task and sets running false and closes repo connection`() {
        //arrange
        whenever(viewStateStorage.isServiceRunning).thenReturn(true)
        //act
        locationTrackStopper()
        //assert
        verify(trackingBgTask).stop()
        verify(viewStateStorage).isServiceRunning = false
        verify(locationRepository).close()
    }

    @Test
    fun `test when service not running only closes repo connection`() {
        //act
        locationTrackStopper()
        //assert
        verify(locationRepository).close()
    }
}
