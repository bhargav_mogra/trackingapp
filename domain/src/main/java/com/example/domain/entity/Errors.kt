package com.example.domain.entity

sealed class LocationError : Exception()

class GpsProviderNotAvailableError : LocationError()

class LocationPermissionNotGrantedError : LocationError()

class UnknownLocationError: LocationError()
