package com.example.domain.background.tasks

interface LocationTrackingBackgroundTask {
    fun start()
    fun stop()
}
