package com.example.data

import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import com.evernote.android.job.util.support.PersistableBundleCompat
import com.example.data.LocationHistorySyncJob.Companion.LOCATION_HISTORY
import com.example.domain.background.tasks.LocationSyncerBackgroundTask
import com.example.domain.entity.Location
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName

class LocationSyncStarter : LocationSyncerBackgroundTask {
    // can inject but lazy
    private val gson = GsonBuilder().serializeNulls().create()

    override fun syncLocationTrackHistory(locationHistory: List<List<Location>>) {
        val bundle = PersistableBundleCompat()
        bundle.putString(
            LOCATION_HISTORY,
            gson.toJson(locationHistory.toSerializables())
        )
        JobRequest.Builder(LocationHistorySyncJob.TAG)
            .addExtras(bundle)
            .startNow()
            .build()
    }

}


class LocationHistorySyncJob : Job() {
    override fun onRunJob(params: Params): Result {
        val locationHistory = params.extras.getString(LOCATION_HISTORY, null)
        locationHistory?.let {
            // Upload to server!!
        }
        return Result.SUCCESS
    }

    companion object {
        const val TAG = "LocationHistorySyncJob"
        const val LOCATION_HISTORY = "locationHistory"
    }
}

private fun List<List<Location>>.toSerializables() = map { it.toSerializable() }

private fun List<Location>.toSerializable() = map { it.toSerializable() }

private fun Location.toSerializable() = SerializableLocation(lat, long)

data class SerializableLocation(
    @SerializedName("latitude")
    val lat: Double,
    @SerializedName("longitude")
    val long: Double
)
