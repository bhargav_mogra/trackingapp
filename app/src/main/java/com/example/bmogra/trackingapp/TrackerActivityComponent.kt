package com.example.bmogra.trackingapp

import dagger.Subcomponent

@Subcomponent(modules = [TrackerActivityModule::class])
@ActivityScope
interface TrackerActivityComponent {
    fun inject(activity: TrackerActivity)
}
