package com.example.presenter

interface TrackerView {
    fun showLastKnownLocation(lastKnownLocation: LocationViewModel)
    fun getUiThreadExecutor(): (Runnable) -> Unit
    fun drawLineOnMap(viewModel: List<LocationViewModel>)
    fun showGpsNotEnabledError()
    fun askForLocationPermission()
    fun clearMap()
    fun updateToStateRecievingLocationUpdates()
}

data class LocationViewModel(val lat: Double, val long: Double)
