package com.example.domain

import com.example.domain.data.contract.LocationRepository
import com.example.domain.data.contract.ViewStateRepository
import com.example.domain.entity.Location
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ViewReadyHandlerTest {
    private lateinit var viewReadyHandler: ViewReadyHandler
    @Mock
    lateinit var locationRepository: LocationRepository
    @Mock
    lateinit var viewStateRepository: ViewStateRepository

    @Before
    fun setUp() {
        viewReadyHandler = ViewReadyHandler(viewStateRepository, locationRepository)
    }

    @Test
    fun `test invocation when service is running tells view to attach itself to location updates`() {
        // Arrange
        whenever(viewStateRepository.isServiceRunning).thenReturn(true)
        whenever(locationRepository.getLastKnownLocation()).thenReturn(null)
        var recieveLocationUpdates = false
        var recieveLastKnownLocation = false
        // ACT
        viewReadyHandler {
            onReceiveLocationUpdates {
                recieveLocationUpdates = true
            }
            onReceiveLastKnownLocation {
                recieveLastKnownLocation = true
            }
        }
        // ASSERT
        assertThat(recieveLocationUpdates).isTrue()
        assertThat(recieveLastKnownLocation).isFalse()
    }

    @Test
    fun `test invocation when service is running tells view to attach itself to location updates and sends lastknownlocation`() {
        // Arrange
        whenever(viewStateRepository.isServiceRunning).thenReturn(true)
        whenever(locationRepository.getLastKnownLocation()).thenReturn(Location(100.0, 13.0))
        var recieveLocationUpdates = false
        var recieveLastKnownLocation = false
        // ACT
        viewReadyHandler {
            onReceiveLocationUpdates {
                recieveLocationUpdates = true
            }
            onReceiveLastKnownLocation {
                recieveLastKnownLocation = true
            }
        }
        // ASSERT
        assertThat(recieveLocationUpdates).isTrue()
        assertThat(recieveLastKnownLocation).isTrue()
    }

    @Test
    fun `test invocation when service not running tells view to move to last known location`() {
        // Arrange
        whenever(viewStateRepository.isServiceRunning).thenReturn(false)
        whenever(locationRepository.getLastKnownLocation()).thenReturn(Location(100.0, 13.0))
        var recieveLocationUpdates = false
        var recieveLastKnownLocation = false
        // ACT
        viewReadyHandler {
            onReceiveLocationUpdates {
                recieveLocationUpdates = true
            }
            onReceiveLastKnownLocation {
                recieveLastKnownLocation = true
            }
        }
        // ASSERT
        assertThat(recieveLocationUpdates).isFalse()
        assertThat(recieveLastKnownLocation).isTrue()
    }

    @Test
    fun `test invocation when service not running and last known loc is null does nothing`() {
        // Arrange
        whenever(viewStateRepository.isServiceRunning).thenReturn(false)
        whenever(locationRepository.getLastKnownLocation()).thenReturn(null)
        var recieveLocationUpdates = false
        var recieveLastKnownLocation = false
        // ACT
        viewReadyHandler {
            onReceiveLocationUpdates {
                recieveLocationUpdates = true
            }
            onReceiveLastKnownLocation {
                recieveLastKnownLocation = true
            }
        }
        // ASSERT
        assertThat(recieveLocationUpdates).isFalse()
        assertThat(recieveLastKnownLocation).isFalse()
    }
}
