package com.example.bmogra.trackingapp

import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {
    fun add(trackerActivityModule: TrackerActivityModule): TrackerActivityComponent

    fun inject(trackingService: TrackingService)
}
