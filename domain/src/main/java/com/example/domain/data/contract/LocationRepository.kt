package com.example.domain.data.contract

import com.example.domain.entity.LocationError
import com.example.domain.entity.Location
import java.io.Closeable

interface LocationRepository : Closeable {
    fun observeLocationUpdates(
        observerId: Int,
        onSuccess: (Location) -> Unit,
        onError: (LocationError) -> Unit
    )

    @Throws(LocationError::class)
    fun getLastKnownLocation(): Location?

    fun getCurrentSessionLocations(): List<Location>

    fun getLocationHistory(): List<List<Location>>

    fun getDistanceBetween(location1: Location, location2: Location): Float

    var minDistanceBeforeNextUpdate: Float
}
