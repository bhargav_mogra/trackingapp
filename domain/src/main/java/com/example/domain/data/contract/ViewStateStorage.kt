package com.example.domain.data.contract

interface ViewStateStorage : ViewStateRepository {
    override var isServiceRunning: Boolean
}
