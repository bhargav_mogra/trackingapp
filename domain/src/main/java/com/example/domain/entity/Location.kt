package com.example.domain.entity

data class Location(val lat: Double, val long: Double)
