package com.example.bmogra.trackingapp

import com.example.data.AndroidLocationRepository
import com.example.data.LocationSyncStarter
import com.example.data.ViewStateStorageImpl
import com.example.domain.HandleViewReady
import com.example.domain.StartTrackingLocation
import com.example.domain.StopTrackingLocation
import com.example.domain.UseCaseFactory
import com.example.domain.data.contract.LocationRepository
import com.example.domain.data.contract.ViewStateStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val trackerApp: TrackerApplication) {
    @Provides
    @Singleton
    fun provideLocationRepository(): LocationRepository =
        AndroidLocationRepository(trackerApp)

    @Provides
    @Singleton
    fun provideUseCaseFactory(
        locationRepository: LocationRepository,
        viewStateStorage: ViewStateStorage
    ): UseCaseFactory {
        return UseCaseFactory(
            locationRepository,
            viewStateStorage,
            LocationTrackingBackgroundTaskImpl(trackerApp),
            LocationSyncStarter()
        )
    }

    @Provides
    @Singleton
    fun provideViewStateStorage(): ViewStateStorage = ViewStateStorageImpl()


    @Provides
    fun startTrackingLocationUseCase(
        useCaseFactory: UseCaseFactory
    ): StartTrackingLocation = useCaseFactory.startTrackingLocationUseCase()

    @Provides
    fun getLastKnownLocationUseCase(
        useCaseFactory: UseCaseFactory
    ): HandleViewReady = useCaseFactory.handleViewReady()

    @Provides
    fun stopTrackingLocationUseCase(
        useCaseFactory: UseCaseFactory
    ): StopTrackingLocation = useCaseFactory.stopTrackingLocationUseCase()
}
