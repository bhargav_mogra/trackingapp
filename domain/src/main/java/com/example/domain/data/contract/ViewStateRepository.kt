package com.example.domain.data.contract

interface ViewStateRepository {
    val isServiceRunning: Boolean
}
