package com.example.bmogra.trackingapp

import android.app.Application

class TrackerApplication : Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appModule(AppModule(this))
            .build()
    }
}
