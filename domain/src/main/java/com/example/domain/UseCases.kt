package com.example.domain

import com.example.domain.background.tasks.LocationSyncerBackgroundTask
import com.example.domain.background.tasks.LocationTrackingBackgroundTask
import com.example.domain.data.contract.LocationRepository
import com.example.domain.data.contract.ViewStateRepository
import com.example.domain.data.contract.ViewStateStorage
import com.example.domain.entity.GpsProviderNotAvailableError
import com.example.domain.entity.Location
import com.example.domain.entity.LocationPermissionNotGrantedError

class UseCaseFactory(
    locationRepository: LocationRepository,
    viewStateStorage: ViewStateStorage,
    trackingBgTask: LocationTrackingBackgroundTask,
    syncerBgTask: LocationSyncerBackgroundTask
) {
    private val locationTracker: LocationTracker =
        LocationTracker(locationRepository, trackingBgTask, viewStateStorage)

    private val handleViewReady: HandleViewReady =
        ViewReadyHandler(viewStateStorage, locationRepository)

    private val locationTrackStopper: LocationTrackStopper =
        LocationTrackStopper(locationRepository, trackingBgTask, viewStateStorage)

    private val locationHistorySyncer = LocationHistorySyncer(syncerBgTask, locationRepository)

    fun startTrackingLocationUseCase(): StartTrackingLocation = locationTracker

    fun handleViewReady(): HandleViewReady = handleViewReady

    fun stopTrackingLocationUseCase(): StopTrackingLocation = locationTrackStopper

    fun locationHistorySyncUseCase(): SyncLocationHistory = locationHistorySyncer
}

interface StartTrackingLocation {
    operator fun invoke(locationTrackerCallback: StartTrackingCallbackBuilder.() -> Unit)
    operator fun invoke()
}

interface HandleViewReady {
    operator fun invoke(builderFunction: ViewReadyCallbackBuilder.() -> Unit)
}

interface StopTrackingLocation {
    operator fun invoke()
}

interface SyncLocationHistory {
    operator fun invoke()
}

internal class ViewReadyHandler(
    private val viewStateRepo: ViewStateRepository,
    private val locationRepository: LocationRepository
) : HandleViewReady {
    override fun invoke(builderFunction: ViewReadyCallbackBuilder.() -> Unit) {
        val callback = ViewReadyCallbackBuilder().apply(builderFunction).build()
        if (viewStateRepo.isServiceRunning) {
            callback.attachViewToLocationUpdateHandler()
            locationRepository.getLastKnownLocation()?.let { callback.moveToShowLastKnownLocation(it) }
        } else {
            locationRepository.getLastKnownLocation()?.let {
                callback.moveToShowLastKnownLocation(it)
            }
        }
    }
}

internal class LocationHistorySyncer(
    private val syncerBgTask: LocationSyncerBackgroundTask,
    private val locationRepository: LocationRepository
) : SyncLocationHistory {
    override fun invoke() {
        syncerBgTask.syncLocationTrackHistory(locationRepository.getLocationHistory())
    }
}

internal class LocationTracker(
    private val locationRepository: LocationRepository,
    private val trackingBgTask: LocationTrackingBackgroundTask,
    private val viewStateStorage: ViewStateStorage
) : StartTrackingLocation {
    init {
        locationRepository.minDistanceBeforeNextUpdate = 1f
    }

    override fun invoke(locationTrackerCallback: StartTrackingCallbackBuilder.() -> Unit) {
        val callback = StartTrackingCallbackBuilder().apply(locationTrackerCallback).build()
        if (!viewStateStorage.isServiceRunning) {
            trackingBgTask.start()
            viewStateStorage.isServiceRunning = true
        }
        locationRepository.observeLocationUpdates(101, {
            callback.onLocationChangedHandler(locationRepository.getCurrentSessionLocations())
        }, {
            when (it) {
                is GpsProviderNotAvailableError -> callback.gpsErrorHandler(it)
                is LocationPermissionNotGrantedError -> callback.locationPermissionErrorHandler(it)
            }
        })
    }

    override fun invoke() {
        locationRepository.observeLocationUpdates(102, {}, {})
    }
}

internal class LocationTrackStopper(
    private val locationRepository: LocationRepository,
    private val trackingBgTask: LocationTrackingBackgroundTask,
    private val viewStateStorage: ViewStateStorage
) : StopTrackingLocation {
    override fun invoke() {
        if (viewStateStorage.isServiceRunning) {
            trackingBgTask.stop()
            viewStateStorage.isServiceRunning = false
        }
        locationRepository.close()
    }
}

class StartTrackingCallbackBuilder {
    private lateinit var gpsErrorHandler: (GpsProviderNotAvailableError) -> Unit
    private lateinit var locationPermissionErrorHandler: (LocationPermissionNotGrantedError) -> Unit
    private lateinit var onLocationChangedHandler: (List<Location>) -> Unit


    fun onGpsNotEnabled(function: (GpsProviderNotAvailableError) -> Unit) {
        gpsErrorHandler = function
    }

    fun onLocationAccessPermissionNotGranted(function: (LocationPermissionNotGrantedError) -> Unit) {
        locationPermissionErrorHandler = function
    }

    fun onLocationChanged(function: (List<Location>) -> Unit) {
        onLocationChangedHandler = function
    }

    internal fun build(): StartTrackingCallback =
        StartTrackingCallback(gpsErrorHandler, locationPermissionErrorHandler, onLocationChangedHandler)
}

internal data class StartTrackingCallback(
    val gpsErrorHandler: (GpsProviderNotAvailableError) -> Unit,
    val locationPermissionErrorHandler: (LocationPermissionNotGrantedError) -> Unit,
    val onLocationChangedHandler: (List<Location>) -> Unit
)

internal data class ViewReadyCallback(
    val attachViewToLocationUpdateHandler: () -> Unit,
    val moveToShowLastKnownLocation: (Location) -> Unit
)

class ViewReadyCallbackBuilder {
    private lateinit var attachViewToLocationUpdateHandler: () -> Unit
    private lateinit var moveToShowLastKnownLocation: (Location) -> Unit

    fun onReceiveLocationUpdates(function: () -> Unit) {
        attachViewToLocationUpdateHandler = function
    }

    fun onReceiveLastKnownLocation(function: (Location) -> Unit) {
        moveToShowLastKnownLocation = function
    }

    internal fun build(): ViewReadyCallback =
        ViewReadyCallback(attachViewToLocationUpdateHandler, moveToShowLastKnownLocation)
}
