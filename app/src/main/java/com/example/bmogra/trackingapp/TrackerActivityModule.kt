package com.example.bmogra.trackingapp

import com.example.domain.HandleViewReady
import com.example.domain.StartTrackingLocation
import com.example.domain.StopTrackingLocation
import com.example.presenter.TrackerPresenter
import dagger.Module
import dagger.Provides

@Module
class TrackerActivityModule {
    @Provides
    @ActivityScope
    fun provideTrackerPresenter(
        startTrackingLocation: StartTrackingLocation,
        stopTrackingLocation: StopTrackingLocation,
        handleViewReady: HandleViewReady
    ): TrackerPresenter = TrackerPresenter(
        startTrackingLocation,
        stopTrackingLocation,
        handleViewReady
    )
}
