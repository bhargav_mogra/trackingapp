package com.example.domain.background.tasks

import com.example.domain.entity.Location

interface LocationSyncerBackgroundTask {
    fun syncLocationTrackHistory(locationHistory: List<List<Location>>)
}
