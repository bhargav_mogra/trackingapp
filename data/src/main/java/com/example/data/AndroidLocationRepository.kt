package com.example.data

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.example.domain.data.contract.LocationRepository
import com.example.domain.entity.GpsProviderNotAvailableError
import com.example.domain.entity.Location
import com.example.domain.entity.LocationError
import com.example.domain.entity.LocationPermissionNotGrantedError

class AndroidLocationRepository(
    private val context: Context
) : LocationRepository {
    private val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private val locationHistory: MutableList<List<Location>> = mutableListOf()
    private var currentSession: LocationTrackingSession? = null
    override var minDistanceBeforeNextUpdate: Float = 1f

    override fun getDistanceBetween(location1: Location, location2: Location): Float =
        location1.toAndroidLocation().distanceTo(location2.toAndroidLocation())

    override fun getCurrentSessionLocations(): List<Location> =
        currentSession?.locationSessionCache?.toList() ?: emptyList()

    override fun getLocationHistory(): List<List<Location>> = locationHistory.toList()

    @Throws(Error::class)
    override fun getLastKnownLocation(): Location? {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            throw LocationPermissionNotGrantedError()
        }
        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)?.toDomainModel()
    }

    override fun observeLocationUpdates(
        observerId: Int,
        onSuccess: (Location) -> Unit,
        onError: (LocationError) -> Unit
    ) {
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            onError(LocationPermissionNotGrantedError())
        } else {
            if (currentSession == null) {
                currentSession = LocationTrackingSession()
                locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    0,
                    minDistanceBeforeNextUpdate,
                    currentSession
                )
            }
            currentSession?.apply {
                add(observerId, onSuccess, onError)
            }
        }
    }

    override fun close() {
        currentSession?.let {
            locationManager.removeUpdates(it)
            locationHistory.add(it.locationSessionCache)
        }
        currentSession = null
    }
}

private class LocationTrackingSession : LocationListener {
    private val listeners: MutableMap<Int, Pair<(Location) -> Unit, (LocationError) -> Unit>> = mutableMapOf()
    val locationSessionCache: MutableList<Location> = mutableListOf()

    override fun onLocationChanged(location: android.location.Location?) {
        location?.apply {
            locationSessionCache.add(location.toDomainModel())
            listeners.toList().forEach {
                it.second.first(toDomainModel())
            }
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
        listeners.toList().forEach {
            it.second.second(GpsProviderNotAvailableError())
        }
    }

    fun add(
        observerId: Int,
        onSuccess: (Location) -> Unit,
        onError: (LocationError) -> Unit
    ) {
        listeners[observerId] = onSuccess to onError
    }
}

private fun android.location.Location.toDomainModel(): Location = Location(latitude, longitude)

private fun Location.toAndroidLocation(): android.location.Location =
    android.location.Location("").also {
        it.latitude = lat
        it.longitude = long
    }
